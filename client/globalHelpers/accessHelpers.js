Template.registerHelper('isOwner',function(){
  var course = Courses.findOne(Session.get("courseSelected"));
  return course.owner === Meteor.userId();
});

Template.registerHelper('isCollaborator',function(){
  var course = Courses.findOne(Session.get("courseSelected"));
  return _.contains(course.collaborators, Meteor.userId()) || course.owner === Meteor.userId();
});

Template.registerHelper('isRegister',function(){
  var course = Courses.findOne(Session.get("courseSelected"));
  return _.contains(course.students, Meteor.userId());
});

Template.registerHelper('whoIs',function(userId){
  if (userId == null) {
    return "";
  }
  var owner = Meteor.users.findOne(userId);
  return owner.profile.firstName + ' ' + owner.profile.lastName;
});
