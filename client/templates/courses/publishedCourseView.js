Template.publishedCourseView.helpers({
  activities: function(){
    return Activities.find({course:this._id, published: true, lost: false});
  },
  courseOwner: function(){
    var owner = Meteor.users.findOne(this.owner)
    return owner.profile.firstName + ' ' + owner.profile.lastName;
  },
  topicName: function(targetTopic){
    return Topics.findOne(targetTopic).name;
  },
});

Template.publishedCourseView.events({
  "click .registerCourse": function(event, template){
     var course = this._id;
     Meteor.call("registerStudent", course, Meteor.userId(), function(error, result){
       if(error){
         console.log("error", error);
       }
       if(result){
         console.log("result", result);
       }
     });
  },
  "click .unregisterCourse": function(event, template){
     var course = this._id;
     Meteor.call("unregisterStudent", course, Meteor.userId(), function(error, result){
       if(error){
         console.log("error", error);
       }
       if(result){
         console.log("result", result);
       }
     });
  }
});

Template.activity2.helpers({
  evaluations: function(){
    return Evaluations.find({activity:this._id});
  },
  simple_evaluations: function(){
    return SimpleEvaluations.find({activity: this._id})
  }
});

Template.uploads2.helpers({
  uploads: function(){
    return Uploads.find({activity: this._id});
  },

});

Template.evaluationDetail.helpers({
  paragraphs: function(course){
    var group = Groups.findOne({course:course, students :{$in: [Meteor.userId()]}});
    return Paragraph.find({evaluation: this._id, student: {$in: group.students}});
  },
  finished: function(course){
    var group = Groups.findOne({course:course, students :{$in: [Meteor.userId()]}});
    var n_paragraph = Paragraph.find({group_evaluation: this._id, student: {$in: group.students}, checked: true}).count();
    if (n_paragraph === group.students.length) {
      return true;
    }
    return false;
  }
});
Template.paragraph.helpers({
  isMyParagraph: function(){
    return this.student === Meteor.userId();
  },
  nEvaluators: function(){
    var course = Courses.findOne(this.course);
    var group = Groups.findOne({course:this.course, students :{$in: [Meteor.userId()]}});
    var nCollaborators = course.collaborators.length + 1;
    var nGroup = group.students.length;
    return nCollaborators + nGroup;
  },
  didIEvaluate: function(){
    var score = Scores.findOne({paragraph: this._id, evaluator: Meteor.userId()});
    if (score) {
      return true;
    }
    return false;
  },
  nScores: function(){
    return score = Scores.find({paragraph: this._id}).count();
  },
  getScore: function(){
    var score = Scores.findOne({paragraph: this._id, evaluator: Meteor.userId()});
    if (score) {
      return score.value;
    }
    return 0;
  },
  getTotalScore: function(){
    var scores = Scores.find({paragraph: this._id});
    var totalScore = 0;
    var countCollaborators = 0;
    var countClassMates = 0;
    var scoreTeacher = 0;
    var scoreCollaborators = 0;
    var scoreClassMates = 0;
    var scoreStudent = 0;
    scores.forEach(function(score){
      if (score.rol === "teacher") {
        scoreTeacher = score.value;
      }
      if (score.rol === "student") {
        scoreStudent = score.value;
      }
      if (score.rol === "collaborator") {
        scoreCollaborators += score.value;
        countCollaborators += 1;
      }
      if (score.rol === "classmate") {
        scoreClassMates += score.value;
        countClassMates += 1;
      }
    });

    if (countCollaborators>0){
      totalScore += (scoreCollaborators/countCollaborators * 0.3);
      totalScore += (scoreTeacher * 0.45);
    }
    else{
      totalScore += (scoreTeacher * 0.75);
    }
    if (countClassMates>0){
      totalScore += (scoreClassMates/countClassMates * 0.15);
      totalScore += (scoreStudent * 0.1);
    }
    else{
      totalScore+= scoreStudent * 0.25;
    }
    return Math.round((totalScore)*10)/10;
  },
});

Template.paragraph.events({
  "click .toggleChecked": function () {
    Paragraph.update(this._id, {$set: {
         checked: ! this.checked,
        }
      }
    );
    FlashMessages.sendSuccess("Enviaste una actividad");
  },
  "submit .calificatorForm": function(event, template){
    event.preventDefault();
    var score = event.target.score.value;
    console.log(score);
    if (this.student === Meteor.userId()) {
      var rol = "student";
    }
    else{
      var rol = "classmate";
    }
    Scores.insert({
      paragraph: this._id,
      value: parseFloat(score),
      evaluator: Meteor.userId(),
      rol: rol,
      student: this.student
    });
    FlashMessages.sendSuccess("Enviaste una calificación");
    return false;

  },
  "keydown .editParagraph": function(event, template){
    if (event.keyCode === 13) {
      $(".editParagraph").blur();
    }
  },
  "blur .editParagraph": function(event, template){
    event.preventDefault();
    var newContent = event.target.value;
    if (newContent !== "" && newContent !== this.content) {
      Paragraph.update(this._id, {$set:{content:newContent}});
    }
  },
});
