Template.coursesManager.rendered = function(){
  $('.datepicker').datepicker({
      format: "yyyy-mm-dd"
  });
}

Template.coursesManager.helpers({
  courseSearch: function(){
    return Session.get("courseSearch");
  },
  courseSearchTopic: function(){
    return Session.get("courseSearchTopic");
  },
  courseSearchUser: function(){
    return Session.get("courseSearchUser");
  },

});

Template.course.helpers({
  courseOwner: function(){
    var owner = Meteor.users.findOne(this.owner)
    return owner.profile.firstName + ' ' + owner.profile.lastName;
  },
  topicName: function(targetTopic){
    return Topics.findOne(targetTopic).name;
  }
});

Template.coursesManager.events({
  "submit #createCourse": function(event, template){
    event.preventDefault();

    var newCourse = {
      owner: Meteor.userId(),
      name : event.target.newCourseName.value,
      description: event.target.newCourseDescription.value,
      objetive: event.target.newCourseObjetive.value,
      topic : event.target.newCourseTopic.value,
      level : event.target.newCourseLevel.value,
      //start: event.target.newCourseStart.value,
      //end: event.target.newCourseEnd.value,
    };
    console.log(event.target);
    Meteor.call("createCourse", newCourse, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
      }
    });

    $('.nav-tabs a[href="#list"]').tab('show');
    FlashMessages.sendSuccess("Se creó un curso");

    event.target.newCourseName.value = "";
    event.target.newCourseDescription.value = "";
    event.target.newCourseObjetive.value = "";
    event.target.newCourseStart.value = "";
    event.target.newCourseEnd.value = "";
    return false;
  },
  "submit #courseSearch": function(event, template){
    event.preventDefault();
    var search = event.target.courseSearch.value;
    var topic = event.target.courseSearchTopic.value;
    var user = event.target.courseSearchUser.value;
    if (search) {
      Session.set("courseSearch", search);
    }
    else{
      Session.set("courseSearch", null);
    }
    if (topic !== 'all') {
      Session.set("courseSearchTopic", topic);
    }
    else{
      Session.set("courseSearchTopic", null);
    }
    if (user != 'all') {
      Session.set("courseSearchUser", user);
    }
    else{
      Session.set("courseSearchUser", null);
    }
    return false;
  },
  "click .clearSearch": function(event, template){
    event.preventDefault();
    Session.set("courseSearch", null);
    Session.set("courseSearchTopic", null);
    Session.set("courseSearchUser", null);
  }
});
