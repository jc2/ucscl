Template.courseList.helpers({
  courseSearch: function(){
    return Session.get("courseSearch");
  },
  courseSearchTopic: function(){
    return Session.get("courseSearchTopic");
  },
});


Template.courseList.events({
  "submit #courseSearch": function(event, template){
    event.preventDefault();
    var search = event.target.courseSearch.value;
    var topic = event.target.courseSearchTopic.value;
    if (search) {
      Session.set("courseSearch", search);
    }
    else{
      Session.set("courseSearch", null);
    }
    if (topic !== 'all') {
      Session.set("courseSearchTopic", topic);
    }
    else{
      Session.set("courseSearchTopic", null);
    }
    return false;
  },
  "click .clearSearch": function(event, template){
    event.preventDefault();
    Session.set("courseSearch", null);
    Session.set("courseSearchTopic", null);
  }
});

Template.course2.helpers({
  courseOwner: function(){
    var owner = Meteor.users.findOne(this.owner)
    return owner.profile.firstName + ' ' + owner.profile.lastName;
  },
  topicName: function(targetTopic){
    return Topics.findOne(targetTopic).name;
  }
});

Template.sidebar2.events({
  "click .selectTopic": function(event, template){
     var topic = event.target.getAttribute("data-id");
     if (topic !== 'all') {
       Session.set("courseSearchTopic", topic);
     }
     else{
       Session.set("courseSearchTopic", null);
     }
  }
});
