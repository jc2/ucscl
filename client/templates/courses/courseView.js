Template.courseView.rendered = function(){
  $('.datepicker').datepicker({
    format: "yyyy-mm-dd",
    startDate: "now",
    todayBtn: "linked",
    language: "es",
    daysOfWeekHighlighted: "0,6",
    todayHighlight: true
  });
}
Template.courseView.helpers({
  topicName: function(targetTopic){
    return Topics.findOne(targetTopic).name;
  },
  topics: function(){
    return Topics.find({},{sort: {name: 1}});
  },
  activities: function(){
    return Activities.find({course:this._id});
  },
  courseOwner: function(){
    var owner = Meteor.users.findOne(this.owner);
    return owner.profile.firstName + ' ' + owner.profile.lastName;
  },
  comments: function () {
	    return Comments.find({course: this._id}, {sort: {createdAt: -1}});
	},
  logs: function () {
	    return Logs.find({course: this._id}, {sort: {createdAt: -1}, limit: 50});
	},
  groupsInCourse: function() {
    return Groups.find({course: this._id});
  },
  courseDetails: function(){
    var collaborators = this.collaborators.length + 1;
    var activeComments = Comments.find({course: this._id, checked:{$in: [false, null]}}).count();
    var checkedComments = Comments.find({course: this._id, checked: true}).count();
    var rawActivities = Activities.find({course:this._id, published:{$in: [false, null]},polling:{$in: [false, null]}}).count();
    var pollingActivities = Activities.find({course:this._id, polling:true}).count();
    var goodActivities = Activities.find({course:this._id, published:true, lost:{$in:[false, null]}}).count();
    var badActivities = Activities.find({course:this._id, published:true, lost:true}).count();
    data = {
      collaborators: collaborators,
      activeComments: activeComments,
      checkedComments: checkedComments,
      rawActivities: rawActivities,
      pollingActivities: pollingActivities,
      goodActivities: goodActivities,
      badActivities: badActivities
    }
    return data;
  },
  getEndDate: function(){
    var date = new Date();
    date.setMonth( date.getMonth( ) + 2 );
    return date;
  },
  getStartDate: function(){
    var date = new Date();
    date.setMonth( date.getMonth( ) + 1 );
    return date;
  }
});

Template.courseView.events({
  "click .removeCourse": function(event, template){
    //event.preventDefault();
    var targetCourse = this._id;
    $('#removeCourse').on('hidden.bs.modal', function(e){
      Meteor.call("removeCourse", targetCourse, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Router.go('coursesManager');
          FlashMessages.sendSuccess("Se eliminó el curso");
        }
      });
    }).modal('hide');
  },
  "submit #createActivity": function(event, template){
    event.preventDefault();
    var course = this._id;
    var newActivity = {
      course : course,
      name : event.target.newActivityName.value,
      description : event.target.newActivityDescription.value,
      objetive : event.target.newActivityObjetive.value,
      order : 1
    };
    Meteor.call("createActivity", newActivity, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        Logs.insert({
          course: course,
          text: "Creó un componente",
          createdAt: new Date(),
          user: Meteor.userId(),
        });
      }
    });

    $('.nav-tabs a[href="#activities"]').tab('show');
    FlashMessages.sendSuccess("Se creó un componente");

    event.target.newActivityName.value = "";
    event.target.newActivityDescription.value = "";
    event.target.newActivityObjetive.value = "";
    return false;
  },
  "submit .newComment": function (event, template) {
    var text = event.target.text.value;
    if(text){
      Comments.insert({
        course: this._id,
        text: text,
        createdAt: new Date(),
        author: Meteor.userId(),
      });
      Logs.insert({
        course: this._id,
        text: "Agregó una sugerencia",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      event.target.text.value = "";
      FlashMessages.sendSuccess("Se creó la sugerencia");
    }
    return false;
	},
  "change .editTopic": function(event, template){

    Courses.update(this._id, {$set:{topic:event.target.value}});
    Logs.insert({
      course: Session.get("courseSelected"),
      text: "Editó la categoría del curso",
      createdAt: new Date(),
      user: Meteor.userId(),
    });
    FlashMessages.sendSuccess("Se editó la categoría del curso");
  },
  "change .editLevel": function(event, template){

    Courses.update(this._id, {$set:{level:event.target.value}});
    Logs.insert({
      course: Session.get("courseSelected"),
      text: "Editó el nivel del curso",
      createdAt: new Date(),
      user: Meteor.userId(),
    });
    FlashMessages.sendSuccess("Se editó el nivel del curso");
  },
  "keydown .editName": function(event, template){
    if (event.keyCode === 13) {
      $(".editName").blur();
    }
  },
  "blur .editName": function(event, template){
    event.preventDefault();
    var newName = event.target.value;
    if (newName !== "" && newName !== this.name) {
      Meteor.call("editCourseName", this._id, newName, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Logs.insert({
            course: Session.get("courseSelected"),
            text: "Editó el nombre del curso",
            createdAt: new Date(),
            user: Meteor.userId(),
          });
          FlashMessages.sendSuccess("Se editó el nombre del curso");
        }
      });
    }
  },
  "keydown .editDescription": function(event, template){
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      $(".editDescription").blur();
    }
  },
  "blur .editDescription": function(event, template){
    event.preventDefault();
    var newDescription = event.target.value;
    if (newDescription !== this.description) {
      Meteor.call("editCourseDescription", this._id, newDescription, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Logs.insert({
            course: Session.get("courseSelected"),
            text: "Editó la descripción del curso",
            createdAt: new Date(),
            user: Meteor.userId(),
          });
          FlashMessages.sendSuccess("Se editó la descripción del curso");
        }
      });
    }
  },
  "keydown .editObjetive": function(event, template){
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      $(".editObjetive").blur();
    }
  },
  "blur .editObjetive": function(event, template){
    event.preventDefault();
    var newObjetive = event.target.value;
    if (newObjetive !== this.objetive) {
      Meteor.call("editCourseObjetive", this._id, newObjetive, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Logs.insert({
            course: Session.get("courseSelected"),
            text: "Editó el objetivo del curso",
            createdAt: new Date(),
            user: Meteor.userId(),
          });
          FlashMessages.sendSuccess("Se editó el objetivo del curso");
        }
      });
    }
  },
  "click .publishCourse": function(event, template){
    Courses.update(this._id, {$set: {published: !this.published}});

    var start = template.find("[name=newCourseStart]").value;
    var end = template.find("[name=newCourseEnd]").value;

    if (!this.published) {
      Meteor.call("publishCourse", this._id, new Date(start), new Date(end), function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          FlashMessages.sendSuccess("Se lanzó el curso");
        }
      });
    }
    else{
      Meteor.call("editCourse", this._id, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          FlashMessages.sendSuccess("Se editó el curso");
        }
      });
    }
  },
  "click .launchCourse": function(event, template){
    var course = this._id;
    var end = template.find("[name=newCourseEnd]").value;
    var startDate = new Date();
    Courses.update(this._id, {$set: {launched: true}});
    var count = 1;
    var group_count = 1;
    var students = [];
    for (var i = 0; i < this.students.length; i++) {
      if (count > 3) {
        Groups.insert({
          number: group_count,
          createdAt: new Date,
          course: this._id,
          students: students,
        });
        group_count++;
        count = 1;
        students = [];
      }
      students.push(this.students[i]);
      count++;
    }
    if (count != 1) {
      Groups.insert({
        number: group_count,
        createdAt: new Date,
        course: this._id,
        students: students,
      });
    }
    var total_students = this.students;
    var cursor = Activities.find({course: this._id});
    cursor.forEach(function(activity){
      console.log(activity.name);
      var cursor = Evaluations.find({activity: activity._id});
      cursor.forEach(function(ge){
        console.log(ge.name);
        console.log(total_students);
        for (var i = 0; i < total_students.length; i++) {
          console.log(total_students[i])
          Paragraph.insert({
            course: course,
            evaluation: ge._id,
            student: total_students[i],
            content: "",
            size: ge.value,
          });
        }
      });

    });
    FlashMessages.sendSuccess("Se lanzó el curso");

  },
  "change #groupSelector": function(event, template){
    var group = event.target.value;
    if (group != "none") {
      Session.set("groupSelected", group);

    }
  }
});
Template.collaborators.helpers({
  usersList: function(){
    return Meteor.users.find({roles:'teacher'});
  },
  invited: function(){
    return Meteor.users.find({_id:{$in:this.collaborators}});
  },
});

Template.collaborators.events({
  "click .inviteUser": function(event, template){
    var course = Session.get("courseSelected");
    var user = template.find("#userToInvite").value;
    Meteor.call("inviteUser", course, user, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result 1", result);
        Logs.insert({
          course: course,
          text: "Invitó a un nuevo colaborador",
          createdAt: new Date(),
          user: Meteor.userId(),
        });
        FlashMessages.sendSuccess("Se invitó a un colaborador");
        return;
      }
    });
    var targetEmail = Meteor.users.findOne(user).emails[0].address;
    var url = window.location.href;
    Meteor.call("sendInvitationEmail", targetEmail, url ,function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log('mail sended');
      }
    });
  },
});

Template.collaborator.events({
  "click .removeUser": function(event, template){
     var course = Session.get("courseSelected");
     var user = this._id;
     $('#removeUser-'+user).on('hidden.bs.modal', function(e){
       Meteor.call("removeUser", course, user, function(error, result){
         if(error){
           console.log("error", error);
         }
         if(result){
           console.log("result", result);
           Logs.insert({
             course: course,
             text: "Rechazó a un colaborador",
             createdAt: new Date(),
             user: Meteor.userId(),
           });
           FlashMessages.sendSuccess("Se rechazó a un colaborador");
         }
       });
     }).modal('hide');

  }
});

Template.students.helpers({
  studentsInCourse: function(){
    return Meteor.users.find({_id:{$in:this.students}});
  },
});

Template.activity.events({
  "submit #createEvaluation": function (event, template){
    event.preventDefault();
    var type = event.target.newEvaluationType.value;
    if (type === "document") {
      Evaluations.insert({
        activity: this._id,
        name: event.target.newEvaluationName.value,
        description: event.target.newEvaluationDescription.value,

      });
      Logs.insert({
        course: Session.get("courseSelected"),
        text: "Creó una evaluación de documento colaborativo",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      FlashMessages.sendSuccess("Se creó una evaluación de documento colaborativo");
    }
    if (type === "free_activity") {
      SimpleEvaluations.insert({
        activity: this._id,
        name: event.target.newEvaluationName.value,
        description: event.target.newEvaluationDescription.value,

      });
      Logs.insert({
        course: Session.get("courseSelected"),
        text: "Creó una evaluación de actividad libre",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      FlashMessages.sendSuccess("Se creó una evaluación de actividad libre");
    }
    event.target.newEvaluationName.value = "";
    event.target.newEvaluationDescription.value = "";
    //event.target.newEvaluationValue.value =

    return false;
  },
  "keydown .editName": function(event, template){
    if (event.keyCode === 13) {
      $(".editName").blur();
    }
  },
  "blur .editName": function(event, template){
    event.preventDefault();
    var newName = event.target.value;
    if (newName !== "" && newName !== this.name) {
      Meteor.call("editActivityName", this._id, newName, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Logs.insert({
            course: Session.get("courseSelected"),
            text: "Editó el nombre de un componente",
            createdAt: new Date(),
            user: Meteor.userId(),
          });
          FlashMessages.sendSuccess("Se editó el nombre de un componente");
        }
      });
    }
  },
  "keydown .editDescription": function(event, template){
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      $(".editDescription").blur();
    }
  },
  "blur .editDescription": function(event, template){
    event.preventDefault();
    var newDescription = event.target.value;
    if (newDescription !== this.description) {
      Meteor.call("editActivityDescription", this._id, newDescription, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Logs.insert({
            course: Session.get("courseSelected"),
            text: "Editó la descripción de un componente",
            createdAt: new Date(),
            user: Meteor.userId(),
          });
          FlashMessages.sendSuccess("Se editó la descripción de un componente");
        }
      });
    }
  },
  "keydown .editObjetive": function(event, template){
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      $(".editObjetive").blur();
    }
  },
  "blur .editObjetive": function(event, template){
    event.preventDefault();
    var newObjetive = event.target.value;
    if (newObjetive !== this.objetive) {
      Meteor.call("editActivityObjetive", this._id, newObjetive, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Logs.insert({
            course: Session.get("courseSelected"),
            text: "Editó el objetivo de un componente",
            createdAt: new Date(),
            user: Meteor.userId(),
          });
          FlashMessages.sendSuccess("Se editó el objetivo de un componente");
        }
      });
    }
  },
  "click .removeActivity": function(event, template){
    //event.preventDefault();
    var targetActivity = this._id;
    $('#removeActivity-'+targetActivity).on('hidden.bs.modal', function(e){
      Meteor.call("removeActivity", targetActivity, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          Logs.insert({
            course: Session.get("courseSelected"),
            text: "Eliminó un componente",
            createdAt: new Date(),
            user: Meteor.userId(),
          });
          FlashMessages.sendSuccess("Se eliminó un componente");
        }
      });
    }).modal('hide');
    /*Meteor.call("removeActivity", this._id, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        Logs.insert({
          course: Session.get("courseSelected"),
          text: "Eliminó un componente",
          createdAt: new Date(),
          user: Meteor.userId(),
        });
        FlashMessages.sendSuccess("Se eliminó un componente");
      }
    });*/

  },
  "click .startPolling": function(event, template){
    event.preventDefault();
    Meteor.call("startPolling", this._id, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        Logs.insert({
          course: Session.get("courseSelected"),
          text: "Empezó la votación componente",
          createdAt: new Date(),
          user: Meteor.userId(),
        });
        FlashMessages.sendSuccess("Se empezó la votación de un componente");
      }
    });
  },
  "click .stopPolling": function(event, template){
    event.preventDefault();
    Meteor.call("stopPolling", this._id, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        Logs.insert({
          course: Session.get("courseSelected"),
          text: "Canceló la votación componente",
          createdAt: new Date(),
          user: Meteor.userId(),
        });
        FlashMessages.sendSuccess("Se canceló la votación de un componente");
      }
    });
  },
  "click .editActivity": function(event, template){
    event.preventDefault();
    Meteor.call("editActivity", this._id, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        Logs.insert({
          course: Session.get("courseSelected"),
          text: "Estableció la edición de un componente",
          createdAt: new Date(),
          user: Meteor.userId(),
        });
        FlashMessages.sendSuccess("Se estableció la edición de un componente");
      }
    });
  },
  "click .voteUp": function(event, template){
    event.preventDefault();
    var course = Courses.findOne(Session.get("courseSelected"));
    var activity = this;
    Meteor.call("voteUp", this._id, Meteor.userId(), function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        if (result === 1) {
          if ( activity.upVoters.length + 1 >= Math.floor(( (course.collaborators.length+1) / 2)+ 1)) {
            Meteor.call("finishPolling", activity._id, function(error, result){
              if(error){
                console.log("error", error);
              }
              if(result){
                console.log("result", result);
                Logs.insert({
                  course: Session.get("courseSelected"),
                  text: "Un componente ha sido escogido favorablemente en la votación",
                  createdAt: new Date(),
                  user: Meteor.userId(),
                });
                FlashMessages.sendSuccess("Se votó a favor de un componente");
              }
            });
          }
        }
      }
    });

  },
  "click .voteDown": function(event, template){
    event.preventDefault();
    var course = Courses.findOne(Session.get("courseSelected"));
    var activity = this;
    Meteor.call("voteDown", this._id, Meteor.userId(), function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        if (result === 1) {
          if ( activity.downVoters.length + 1 >= Math.floor(( (course.collaborators.length+1) / 2)+ 1)) {
            Meteor.call("lostPolling", activity._id, function(error, result){
              if(error){
                console.log("error", error);
              }
              if(result){
                console.log("result", result);
                Logs.insert({
                  course: Session.get("courseSelected"),
                  text: "Un componente ha fallado en la votación",
                  createdAt: new Date(),
                  user: Meteor.userId(),
                });
                FlashMessages.sendSuccess("Se votó a en contra de un componente");
              }
            });
          }
        }
      }
    });


  },
});

Template.activity.helpers({
  isVoterUp: function(){
    return _.contains(this.upVoters, Meteor.userId());
  },
  isVoterDown: function(){
    return _.contains(this.downVoters, Meteor.userId());
  },
  votersUpCount: function(){
    return this.upVoters.length;
  },
  votersDownCount: function(){
    return this.downVoters.length;
  },
  evaluations: function(){
    return Evaluations.find({activity: this._id})
  },
  simple_evaluations: function(){
    return SimpleEvaluations.find({activity: this._id})
  }
});

Template.uploads.helpers({
  uploads: function(){
    return Uploads.find({activity: this._id});
  },
  links: function(){
    return Links.find({activity: this._id});
  },
});

Template.uploads.events({
  "submit .createLink": function(event, template){
    var name = event.target.linkName.value;
    var url = event.target.linkURL.value;

    if (url && name) {
      Links.insert({
        name: name,
        url: url,
        activity: this._id,
      });
      Logs.insert({
        course: Session.get("courseSelected"),
        text: "Agregó un link a un componente",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      FlashMessages.sendSuccess("Se agregó un Link");
    }
    return false;

  },
  "change .fileInput": function(event, template){
    event.preventDefault();
    var activity = this._id;
    FS.Utility.eachFile(event, function(file){
      var newUpload = new FS.File(file);
      newUpload.activity = activity;
      Uploads.insert(newUpload, function(err){
        console.log(err);
      });
      Logs.insert({
        course: Session.get("courseSelected"),
        text: "Agregó un archivo a un componente",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      FlashMessages.sendSuccess("Se agregó un archivo");
    });
    return false;
  },
  "click .removeUpload": function(event, template){
    event.preventDefault();
    Meteor.call("removeUpload", this._id, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        Logs.insert({
          course: Session.get("courseSelected"),
          text: "Eliminó un recurso de un componente",
          createdAt: new Date(),
          user: Meteor.userId(),
        });
        FlashMessages.sendSuccess("Se eliminó un recurso");
      }
    });
  },
  "click .removeLink": function(event, template){
    event.preventDefault();
    Links.remove({_id:this_id});
    Logs.insert({
      course: Session.get("courseSelected"),
      text: "Eliminó un recurso de un componente",
      createdAt: new Date(),
      user: Meteor.userId(),
    });
    FlashMessages.sendSuccess("Se eliminó un recurso");
  }
});

Template.comment.events({
  "click .toggleChecked": function () {
    Comments.update(this._id, {$set: {
         checked: ! this.checked,
        }
      }
    );
    if (!this.checked) {
      Logs.insert({
        course: Session.get("courseSelected"),
        text: "Marcó una sugerencia como hecha",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      FlashMessages.sendSuccess("Se marcó una sugerencia como hecha");
    }
    else{
      Logs.insert({
        course: Session.get("courseSelected"),
        text: "Marcó una sugerencia como incompleta",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      FlashMessages.sendSuccess("Se marcó una sugerencia como incompleta");
    }
  },
  "click .removeComment": function () {
    var targetComment = this._id;
    $('#removeComment-'+targetComment).on('hidden.bs.modal', function(e){
      Comments.remove(targetComment);
      Logs.insert({
        course: Session.get("courseSelected"),
        text: "Eliminó una sugerencia",
        createdAt: new Date(),
        user: Meteor.userId(),
      });
      FlashMessages.sendSuccess("Se eliminó una sugerencia");
    }).modal('hide');
  },
});

Template.comment.helpers({
  isAuthor: function(){
    return this.author === Meteor.userId();
  }
});

Template.group.helpers({
  studentsInGroup: function(){
    return Meteor.users.find({_id:{$in:this.students}});
  }
});

Template.evaluation.events({
  "click .deleteEvaluation": function(event, template){
    Evaluations.remove(this._id);
    Logs.insert({
      course: Session.get("courseSelected"),
      text: "Eliminó una evaluación",
      createdAt: new Date(),
      user: Meteor.userId(),
    });
    FlashMessages.sendSuccess("Se eliminó una evaluación");
  }
});

Template.evaluationCal.helpers({
  isGroupSelected: function(){
    return Session.get("groupSelected") == null? false:true;
  },
  studentsInGroup: function(){
    var group = Groups.findOne(Session.get("groupSelected"));
    if (group) {
      return Meteor.users.find({_id:{$in:group.students}});
    }
  },
  activities: function(){
    //var course = Courses.findOne(Session.get("courseSelected"));
    return Activities.find({course:Session.get("courseSelected"), published: true, lost: false});
  },

});

Template.activity3.helpers({
  evaluations: function(){
    return Evaluations.find({activity:this._id});
  },
  simple_evaluations: function(){
    return SimpleEvaluations.find({activity: this._id})
  }
});
Template.evaluationDetail2.helpers({
  paragraphs: function(){
    var group = Groups.findOne(Session.get("groupSelected"));
    if (group) {
      return Paragraph.find({evaluation: this._id, student: {$in: group.students}});
    }
  },
  finished: function(course){
    var group = Groups.findOne({course:course, students :{$in: [Meteor.userId()]}});
    var n_paragraph = Paragraph.find({group_evaluation: this._id, student: {$in: group.students}, checked: true}).count();
    if (n_paragraph === group.students.length) {
      return true;
    }
    return false;
  }
});

Template.paragraph2.helpers({
  isMyParagraph: function(){
    return this.student === Meteor.userId();
  },
  nEvaluators: function(){
    var course = Courses.findOne(this.course);
    var group = Groups.findOne(Session.get("groupSelected"));
    var nCollaborators = course.collaborators.length + 1;
    var nGroup = group.students.length;
    return nCollaborators + nGroup;
  },
  didIEvaluate: function(){
    var score = Scores.findOne({paragraph: this._id, evaluator: Meteor.userId()});
    if (score) {
      return true;
    }
    return false;
  },
  nScores: function(){
    return score = Scores.find({paragraph: this._id}).count();
  },
  getScore: function(){
    var score = Scores.findOne({paragraph: this._id, evaluator: Meteor.userId()});
    if (score) {
      return score.value;
    }
    return 0;
  },
  getTotalScore: function(){
    var scores = Scores.find({paragraph: this._id});
    var totalScore = 0;
    var countCollaborators = 0;
    var countClassMates = 0;
    var scoreTeacher = 0;
    var scoreCollaborators = 0;
    var scoreClassMates = 0;
    var scoreStudent = 0;
    scores.forEach(function(score){
      if (score.rol === "teacher") {
        scoreTeacher = score.value;
      }
      if (score.rol === "student") {
        scoreStudent = score.value;
      }
      if (score.rol === "collaborator") {
        scoreCollaborators += score.value;
        countCollaborators += 1;
      }
      if (score.rol === "classmate") {
        scoreClassMates += score.value;
        countClassMates += 1;
      }
    });

    if (countCollaborators>0){
      totalScore += (scoreCollaborators/countCollaborators * 0.3);
      totalScore += (scoreTeacher * 0.45);
    }
    else{
      totalScore += (scoreTeacher * 0.75);
    }
    if (countClassMates>0){
      totalScore += (scoreClassMates/countClassMates * 0.15);
      totalScore += (scoreStudent * 0.1);
    }
    else{
      totalScore+= scoreStudent * 0.25;
    }
    return Math.round((totalScore)*10)/10;
  },
});

Template.paragraph2.events({
  "submit .calificatorForm": function(event, template){
    event.preventDefault();
    var score = event.target.score.value;
    console.log(score);
    var course = Courses.findOne(Session.get("courseSelected"));
    if (_.contains(course.collaborators, Meteor.userId())){
      var rol = "collaborator";
    }
    if (course.owner === Meteor.userId()){
      var rol = "teacher";
    }
    Scores.insert({
      paragraph: this._id,
      value: parseFloat(score),
      evaluator: Meteor.userId(),
      rol: rol,
      student: this.student
    });
    FlashMessages.sendSuccess("Enviaste una calificación");
    return false;

  }
});
