Template.topicsManager.events({
  'submit #createTopic': function (event, template) {
    event.preventDefault();
    var newTopic = {
      name : event.target.newTopicName.value,
    }
    Meteor.call('createTopic', newTopic, function(error, result){
      if(error){
        console.log('error', error);
      }
      if(result){
        console.log('result', result);
        FlashMessages.sendSuccess("Se creó una nueva categoría");
      }
    });
    event.target.newTopicName.value = "";
    return true;
  },
  'click .removeTopic': function(event, template){
    console.log(this._layout);
    Meteor.call("removeTopic", this._id, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
        console.log("result", result);
        FlashMessages.sendSuccess("Se eliminó una categoría");
      }
    });
  }
});

Template.topic.events({
  "keydown .editName": function(event, template){
    if (event.keyCode === 13) {
      $(".editName").blur();
    }
  },
  "blur .editName": function(event, template){
    event.preventDefault();
    var newName = event.target.value;
    if (newName !== "" && newName !== this.name) {
      Meteor.call("editTopicName", this._id, newName, function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
          console.log("result", result);
          FlashMessages.sendSuccess("Se editó el nombre del la categoría");
        }
      });
    }
  },
});
