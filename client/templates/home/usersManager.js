Template.usersManager.helpers({
  getRol: function(targetUser){
    var roles =  Roles.getRolesForUser(targetUser);
    if (roles.length > 0) {
      return roles[0]
    }
    else{
      return 'student'
    }
  }
});

Template.usersManager.events({
  "change .radio-inline": function(event, template){
    var targetUser = $(event.target).attr("data-user");
    var newRol = event.target.value;
    Meteor.call("changeRol", targetUser, newRol, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){

      }
    });
    FlashMessages.sendSuccess("Rol actualizado en el usuario");
  }
});
