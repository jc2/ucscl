Template.profile.events({
  "submit #editProfile": function(event, template){
     var newFirstName = event.target.newFirstName.value;
     var newLastName = event.target.newLastName.value;
     if (newFirstName != ""){
       Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.firstName":newFirstName}})
     }
     if (newLastName != ""){
       Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.lastName":newLastName}})
     }
     return false;
  }
});
