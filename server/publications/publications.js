Meteor.publish("topics", function(){
  return Topics.find({});
});
Meteor.publish("courses", function(){
  return Courses.find({});
});
Meteor.publish("activities", function(argument){
  return Activities.find({});
})
Meteor.publish("uploads", function(activity){
  return Uploads.find({activity:activity});
});
Meteor.publish("comments", function(activity){
  return Comments.find({activity:activity});
});
Meteor.publish("logs", function(activity){
  return Logs.find({activity:activity});
});
Meteor.publish("users", function(){
  return Meteor.users.find({});
});
Meteor.publish("paragraph", function(ge){
  return Paragraph.find();
});
