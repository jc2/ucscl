Meteor.methods({
  changeRol: function(targetUser, newRol){
    if (Roles.userIsInRole(Meteor.userId(), 'admin')) {
        Roles.setUserRoles(targetUser, newRol);
    }
  }, 
});
