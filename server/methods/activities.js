Meteor.methods({
  createActivity:function(newActivity){
     newActivity.createdAt = new Date();
     return Activities.insert(newActivity);
  },
  removeActivity:function(targetActivity){
    return Activities.remove(targetActivity);
  },
  startPolling:function(targetActivity){
    Activities.update(targetActivity, {$set :{downVoters: [], upVoters: []}});
    return Activities.update(targetActivity, {$set :{polling: true}});
  },
  stopPolling:function(targetActivity){
    return Activities.update(targetActivity, {$set :{downVoters: [], upVoters: [], polling: false}});
  },
  editActivity:function(targetActivity){
    return Activities.update(targetActivity, {$set :{downVoters: [], upVoters: [], published: false}});
  },
  finishPolling:function(targetActivity){
    return Activities.update(targetActivity, {$set :{polling: false, published: true, lost:false}});
  },
  lostPolling:function(targetActivity){
    return Activities.update(targetActivity, {$set :{polling: false, published: true, lost:true}});
  },
  voteUp:function(targetActivity, user){
    var activity = Activities.findOne(targetActivity);
    if (_.contains(activity.upVoters, user)) {
      return -1;
    }
    Activities.update(targetActivity, {$pull: {downVoters: user}});
    return Activities.update(targetActivity, {$addToSet: {upVoters: user}});
  },
  voteDown:function(targetActivity, user){
    var activity = Activities.findOne(targetActivity);
    if (_.contains(activity.downVoters, user)) {
      return -1;
    }
    Activities.update(targetActivity, {$pull: {upVoters: user}})
    return Activities.update(targetActivity, {$addToSet: {downVoters: user}});
  },
  editActivityName:function(targetActivity, newName){
    return Activities.update(targetActivity, {$set: {name: newName}});
  },
  editActivityDescription:function(targetActivity, newDescription){
    return Activities.update(targetActivity, {$set: {description: newDescription}});
  },
  editActivityObjetive:function(targetActivity, newObjetive){
    return Activities.update(targetActivity, {$set: {objetive: newObjetive}});
  },

});
Activities.before.remove(function (userId, activity) {
  Uploads.remove({activity: activity._id});
  Evaluations.remove({activity: activity._id});
});

Evaluations.before.remove(function (userId, ge) {
  Paragraph.remove({evaluation: ge});
});
