Meteor.methods({
  createTopic:function(newTopic){
    check(newTopic.name, String);
    newTopic.createdAt =  new Date();
    return Topics.insert(newTopic);
  },
  removeTopic:function(targetTopic){
    Topics.remove(targetTopic);
  },
  editTopicName: function(targetTopic, newName){
    return Topics.update(targetTopic, {$set: {name: newName}});
  }
});
