Meteor.methods({
  createCourse:function(newCourse){
    newCourse.createdAt = new Date();
    newCourse.collaborators = [];
    return Courses.insert(newCourse);
  },
  editCourseName:function(targetCourse, newName){
    return Courses.update(targetCourse, {$set: {name: newName}});
  },
  editCourseDescription:function(targetCourse, newDescription){
    return Courses.update(targetCourse, {$set: {description: newDescription}});
  },
  editCourseObjetive:function(targetCourse, newObjetive){
    return Courses.update(targetCourse, {$set: {objetive: newObjetive}});
  },
  removeCourse:function(targetCourse){

    return Courses.remove(targetCourse);
  },
  inviteUser:function(targetCourse, newUser){
    var course = Courses.findOne(targetCourse);
    if (!course || course.owner !== this.userId) {
      throw new Meteor.Error(404);
    }
    if (newUser !== course.owner && !_.contains(course.collaborators, newUser)) {
      return Courses.update(targetCourse, {$addToSet: {collaborators: newUser}});
    }

  },
  removeUser:function(targetCourse, targetUser){
    var course = Courses.findOne(targetCourse);
    if (!course || course.owner !== this.userId) {
      throw new Meteor.Error(404);
    }
    if (_.contains(course.collaborators, targetUser)) {
      return Courses.update(targetCourse, {$pull: {collaborators: targetUser}});
    }
  },
  registerStudent:function(targetCourse, targetUser){
    var course = Courses.findOne(targetCourse);
    if (!course) {
      throw new Meteor.Error(404);
    }
    if (targetUser !== course.owner && !_.contains(course.students, targetUser)) {
      return Courses.update(targetCourse, {$addToSet: {students: targetUser}});
    }
  },
  unregisterStudent:function(targetCourse, targetUser){
    var course = Courses.findOne(targetCourse);
    if (!course) {
      throw new Meteor.Error(404);
    }
    if (_.contains(course.students, targetUser)) {
      return Courses.update(targetCourse, {$pull: {students: targetUser}});
    }
  },
  publishCourse:function(targetCourse, startDate, endDate){
    /*Activities.remove({
      course:targetCourse,
      polling: {$in: [true, null]},
    });
    Activities.remove({
      course:targetCourse,
      published: {$in: [false, null]},
    });
    Activities.remove({
      course:targetCourse,
      lost: {$in: [true, null]}
    });]*/
    Courses.update(targetCourse, {$set: {launched: false, students: [], endDate: endDate, startDate: startDate}});
    Comments.remove({course: targetCourse});
    Logs.remove({course: targetCourse});
    return true;
  },
  editCourse:function(targetCourse){
    Courses.update(targetCourse, {$set: {launched: false, students: [], endDate: null, startDate: null}});
    Groups.remove({course: targetCourse});
    Paragraph.remove({course:targetCourse});
    return true;
  }
});
Courses.before.remove(function (userId, course) {
  Activities.remove({course: course._id});
  Logs.remove({course: course._id});
  Comments.remove({course: course._id});
  Groups.remove({course: course});
});
