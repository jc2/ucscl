Meteor.methods({
  sendInvitationEmail: function (to, url) {
    check([to, url], [String]);
    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();
    Email.send({
      to: to,
      from: "jcceron@unicauca.edu.co",
      subject: "u-CSCL: Te han invitado a participar en un curso colaborativo",
      text: "Te han invitado a participar de un curso, miralo en: " + url
    });
  }
});
