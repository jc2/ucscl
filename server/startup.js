Meteor.startup(function(){
  if(Meteor.users.find().count() < 1 || Roles.getUsersInRole('admin').count() < 1){
    var userObject = {
      username: "admin",
      mail: "admin@mail.com",
      password: "!QAZ2wsx",
      firstName: "Admin",
      lastName: "",
      roles: ["admin"]
    };

    var userId = Accounts.createUser({
       email: userObject.mail,
       password: userObject.password,
       profile: {
         firstName: userObject.firstName,
         lastName: userObject.lastName
       }
    });

    Meteor.users.update(userId, {$set:{"emails.0.verified":true}});
    Roles.addUsersToRoles(userId, userObject.roles);
  }

  process.env.MAIL_URL='smtp://ucscl%40unicauca.edu.co:147258369@smtp.gmail.com:587/'

});
