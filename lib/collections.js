Topics = new Mongo.Collection('topics');
Topics.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Courses = new Mongo.Collection('courses');
Courses.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Activities = new Mongo.Collection('activities');
Activities.allow({
  insert: function(){
    return true;
  },
  update: function(){
    if ((_.contains(course.collaborators, Meteor.userId()) || course.owner === Meteor.userId()) && !this.polling && !this.published) {
      return true;
    }
    else{
      return false;
    }

  },
  remove: function(){
    return true;
  }
});

Uploads = new FS.Collection("uploads", {
  stores: [new FS.Store.FileSystem('uploads', {path: './meteor-uploads'})]
});
Uploads.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  },
  download: function(){
    return true;
  }
});

Links = new Mongo.Collection("links");
Links.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Comments = new Mongo.Collection("comments");
Comments.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Logs = new Mongo.Collection("logs");
Logs.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Groups = new Mongo.Collection("groups");
Groups.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Evaluations = new Mongo.Collection("evaluations");
Evaluations.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

SimpleEvaluations = new Mongo.Collection("simple_evaluations");
SimpleEvaluations.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Paragraph = new Mongo.Collection("paragraph");
Paragraph.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

Scores = new Mongo.Collection("scores");
Scores.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});
