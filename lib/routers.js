Router.configure({
  layoutTemplate: "oneColLayout",
  notFoundTemplate: "notFound",
  loadingTemplate: "loading"
});

Router.route("/", {
  name:"home",
});

Router.route("profile", {
  name:"profile",
});

Router.route('/users-manager', {
  name: 'usersManager',
  layoutTemplate: "twoColLayout",
  waitOn: function() {
    return Meteor.subscribe('users');
  },
  data: {
    'users': function(){
      return Meteor.users.find({"emails.address":{$not: 'admin@mail.com'}});
    }
  },
  onBeforeAction:function(){
    if (Roles.userIsInRole(Meteor.userId(), 'admin')) {
     this.next();
    }
    else {
      this.render('notFound');
    }
  },
});

Router.route('/dashboard', {
  name: 'dashboard',
  layoutTemplate: "twoColLayout",
});

Router.route('/topics-manager', {
  name: 'topicsManager',
  layoutTemplate: "twoColLayout",
  waitOn: function() {
    return Meteor.subscribe('topics');
  },
  data: {
    'topics': function(){
      return Topics.find({},{sort: {name: 1}});
    }
  },
  onAfterAction: function () {
    //Meta.setTitle('Topics');
  },
  onBeforeAction:function(){
    if (Roles.userIsInRole(Meteor.userId(), 'admin')) {
     this.next();
    }
    else {
      this.render('notFound');
    }
  },
});

Router.route('/courses-manager', {
  name: 'coursesManager',
  layoutTemplate: "twoColLayout",
  waitOn: function() {
    Meteor.subscribe('users');
    Meteor.subscribe('topics');
    return Meteor.subscribe('courses');
  },
  data: {
    'courses': function(){
      searchCriteria = {};
      if (Session.get("courseSearch")) {
        searchCriteria.name = {$regex: ".*"+Session.get("courseSearch")+".*"};
      }
      if (Session.get("courseSearchTopic")) {
        searchCriteria.topic = Session.get("courseSearchTopic");
      }
      if (Session.get("courseSearchUser") === 'owner'){
        searchCriteria.owner = Meteor.userId();
      }
      if (Session.get("courseSearchUser") === 'collaborator'){
        searchCriteria.collaborators = Meteor.userId();
      }
      if (Session.get("courseSearchUser") === 'mines') {
        searchCriteria['$or'] = [{owner:Meteor.userId()},{collaborators: Meteor.userId()}];
      }
      return Courses.find(searchCriteria,{sort: {name: 1}});
    },
    'topics': function(){
      return Topics.find({},{sort: {name: 1}});
    }
  },
  onAfterAction: function () {
    //Meta.setTitle('Courses');
  },
  onBeforeAction:function(){
    if (Roles.userIsInRole(Meteor.userId(), 'teacher')) {
     this.next();
    }
    else {
      this.render('notFound');
    }
  },
});

Router.route('/course-list', {
  name: 'courseList',
  layoutTemplate: "twoColLayout2",
  waitOn: function() {
    Meteor.subscribe('users');
    Meteor.subscribe('topics');
    return Meteor.subscribe('courses');
  },
  data: {
    'courses': function(){
      if (Session.get("courseSearch") && Session.get("courseSearchTopic")) {
        return Courses.find({name: {$regex: ".*"+Session.get("courseSearch")+".*"}, topic: Session.get("courseSearchTopic"), published: true},{sort: {name: 1}});
      }
      if (Session.get("courseSearch")) {
        return Courses.find({name: {$regex: ".*"+Session.get("courseSearch")+".*"}, published: true},{sort: {name: 1}});
      }
      if (Session.get("courseSearchTopic")) {
        return Courses.find({topic: Session.get("courseSearchTopic"), published: true},{sort: {name: 1}});
      }
      return Courses.find({published: true},{sort: {name: 1}});
    },
    'topics': function(){
      return Topics.find({},{sort: {name: 1}});
    }
  },
  onAfterAction: function () {
    //Meta.setTitle('Courses');
  }
});

Router.route("/course/:id", {
  name: "courseView",
  layoutTemplate: "twoColLayout",
  waitOn: function() {
    Session.set("courseSelected", this.params.id);
    Meteor.subscribe('users');
    Meteor.subscribe('activities');
    Meteor.subscribe('uploads');
    Meteor.subscribe('comments');
    Meteor.subscribe('logs');
    return Meteor.subscribe('courses');
  },
  data: function(){
    return Courses.findOne({_id: this.params.id});
  },
  onAfterAction: function () {
    //Meta.setTitle('Courses View');
  },
  onBeforeAction:function(){
    if (Roles.userIsInRole(Meteor.userId(), 'teacher')) {
     this.next();
    }
    else {
      this.render('notFound');
    }
  },
});

Router.route("/publishedCourse/:id", {
  name: "publishedCourseView",
  layoutTemplate: "twoColLayout3",
  waitOn: function() {
    Session.set("courseSelected", this.params.id);
    Meteor.subscribe('activities');
    return Meteor.subscribe('courses');
  },
  data: function(){
    return Courses.findOne({_id: this.params.id, published: true});
  },
  onAfterAction: function () {
    //Meta.setTitle('Courses View');
  },

});
