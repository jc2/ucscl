T9n.setLanguage('es');

AccountsTemplates.configure({
    // Behaviour
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: false,
    overrideLoginErrors: false,
    sendVerificationEmail: false,
    lowercaseUsername: false,

    // Appearance
    //defaultLayout: ,
    showAddRemoveServices: true,
    showForgotPasswordLink: true,
    showLabels: true,
    showPlaceholders: false,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: true,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    // Privacy Policy and Terms of Use
    // privacyUrl: 'privacy',
    // termsUrl: 'terms-of-use',

    // Redirects
    homeRoutePath: '/',
    redirectTimeout: 4000,

    // Hooks
    //onLogoutHook: myLogoutFunc,
    //onSubmitHook: mySubmitFunc,
    //preSignUpHook: myPreSubmitFunc,

    // Texts
    // texts: {
    //     navSignIn: "signIn",
    //     navSignOut: "signOut",
    //     optionalField: "optional",
    //     pwdLink_pre: "",
    //     pwdLink_link: "forgotPassword",
    //     pwdLink_suff: "",
    //     resendVerificationEmailLink_pre: "Verification email lost?",
    //     resendVerificationEmailLink_link: "Send again",
    //     resendVerificationEmailLink_suff: "",
    //     sep: "OR",
    //     signInLink_pre: "ifYouAlreadyHaveAnAccount",
    //     signInLink_link: "signin",
    //     signInLink_suff: "",
    //     signUpLink_pre: "dontHaveAnAccount",
    //     signUpLink_link: "signUp",
    //     signUpLink_suff: "",
    //     socialAdd: "add",
    //     socialConfigure: "configure",
    //     socialIcons: {
    //         "meteor-developer": "fa fa-rocket",
    //     },
    //     socialRemove: "remove",
    //     socialSignIn: "signIn",
    //     socialSignUp: "signUp",
    //     socialWith: "with",
    //     termsPreamble: "clickAgree",
    //     termsPrivacy: "privacyPolicy",
    //     termsAnd: "and",
    //     termsTerms: "terms",
    // }
});

AccountsTemplates.configureRoute('signIn', {
    //name: 'signIn',
    ///path: '/login',
    //template: 'myLogin',
    layoutTemplate: 'oneColLayout',
    //redirect: '/user-profile',
});
AccountsTemplates.configureRoute('signUp',{
  layoutTemplate: 'oneColLayout',
});
AccountsTemplates.configureRoute('changePwd',{
  layoutTemplate: 'oneColLayout',
});
AccountsTemplates.configureRoute('ensureSignedIn');

var root_path = (Meteor.settings && Meteor.settings.public && Meteor.settings.public.root_path) || "";
/*AccountsTemplates.configureRoute('changePwd', {
    path: root_path  + "/change-password",
});
AccountsTemplates.configureRoute('enrollAccount', {
    path: root_path  + "/enroll-account",
});
AccountsTemplates.configureRoute('forgotPwd', {
    path: root_path  + "/forgot-password",
});
AccountsTemplates.configureRoute('resetPwd', {
    path: root_path  + "/reset-password",
});
AccountsTemplates.configureRoute('signIn', {
    path: root_path  + "/sign-in",
});
AccountsTemplates.configureRoute('verifyEmail', {
    path: root_path  + "/verify-email",
});*/
AccountsTemplates.configureRoute('resetPwd', {
    path: root_path  + "/reset-password",
    layoutTemplate: 'oneColLayout',
});

Router.plugin('ensureSignedIn', {
  only: ['dashboard', 'topicsManager', 'coursesManager', 'usersManager', 'courseView', 'profile']
});

/*AccountsTemplates.configureRoute('signIn', {
  redirect: function(){
    var user = Meteor.user();

    if (user && Roles.userIsInRole(user, ['admin'])) {
      Router.go('admin');
    }
    else {
      Router.go('home');
    }
  }
});*/


AccountsTemplates.addFields([
    {
      _id: 'firstName',
      type: 'text',
      displayName: 'Nombre',
      required: true,
    },
    {
      _id:'lastName',
      type: 'text',
      displayName: 'Apellido',
      required: true,
    }
]);
